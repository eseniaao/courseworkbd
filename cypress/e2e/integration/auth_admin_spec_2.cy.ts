describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    // открытие страницы авторизации перед каждым тестом
    // cy.visit('localhost:4200/admin');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('wrong');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем, что страница не загрузилась и URL не содержит слово manage
    cy.url().should('not.include', '/manage');

    // проверяем наличие ошибки
    cy.get('.invalidPassword').should('exist');
  });
});
