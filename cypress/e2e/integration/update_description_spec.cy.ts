describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('PATCH', '/api/administration', {
      statusCode: 200,
      body: {
        description: 'goodbye',
      },
    }).as('patchDesc');
    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/breakfasts', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/tours', {
      body: [],
    }).as('getPositions');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.intercept('GET', '/api/description', {
      body: [
        {
          description: 'hello',
        },
      ],
    }).as('getDesc');

    cy.visit('localhost:4200/');
    cy.contains('hello');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('[formControlName="description"]').click().type('{selectall}').type('goodbye');
    cy.get('.submitDesc').click();

    cy.intercept('GET', '/api/description', {
      body: [
        {
          description: 'goodbye',
        },
      ],
    }).as('getDesc');

    cy.visit('localhost:4200/');

    cy.get('@getDesc');

    cy.contains('goodbye');
  });
});
