describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('DELETE', `/api/staff`, {
      body: {
        id: 31,
      },
    }).as('deleteStaff');

    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/tours', {});
    cy.intercept('GET', '/api/breakfasts', {});
    cy.intercept('GET', '/api/bookings', {});
    cy.intercept('GET', '/api/booking-staff', {});
    cy.intercept('GET', '/api/booking-room', {});
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.intercept('GET', '/api/staff', [
      {
        id: 31,
        firstName: 'Santa',
        lastName: 'Lucia',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
    ]).as('getStaff');
    cy.intercept('GET', '/api/staff/31', [
      {
        id: 31,
        firstName: 'Santa',
        lastName: 'Lucia',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
    ]).as('getStaff1');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Santa');
    cy.get('[formControlName="lastName"]').type('Lucia');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем наличие элемента редактирования описания
    cy.get('.deleteStaff').click();

    cy.get('.p-confirm-dialog-accept').click();

    cy.wait('@deleteStaff');

    cy.intercept('GET', '/api/staff', []).as('getStaff');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Santa');
    cy.get('[formControlName="lastName"]').type('Lucia');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем наличие элемента редактирования описания
    cy.get('.invalidUser').should('exist');
  });
});
