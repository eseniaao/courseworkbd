describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    // открытие страницы авторизации перед каждым тестом
    // cy.visit('localhost:4200/admin');
    cy.intercept('POST', '/api/guests', {
      statusCode: 200,
      body: {
        id: 100,
        firstName: 'John',
        lastName: 'Doe',
      },
    }).as('addGuest');

    cy.intercept('GET', '/api/guest/100', { id: 100, firstName: 'John', lastName: 'Doe', rating: null }).as('getGuest');
    cy.intercept('GET', '/api/guests', []).as('getGuests');
    cy.intercept('GET', '/api/guest-booking', []);
    cy.intercept('GET', '/api/bookings', []);
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.visit('localhost:4200/guest');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('John');
    cy.get('[formControlName="lastName"]').type('Doe');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.wait('@addGuest');

    cy.url().should('include', '/guest/100');

    // проверяем наличие элемента редактирования описания
    cy.get('.createNewBooking').should('be.visible').should('not.be.disabled');
    cy.get('.changeCurrentBooking').should('not.exist');
    cy.get('.showPreviousBookings').should('not.exist');
  });
});
