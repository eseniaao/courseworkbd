describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('POST', '/api/breakfasts', {
      statusCode: 200,
      body: {
        id: 50,
        menu: 'unique_breakfast',
        price: 1111,
      },
    }).as('addBreakfast');

    cy.intercept('GET', '/api/breakfasts', [{ id: 50, menu: 'unique_breakfast', price: 1111 }]).as('getBreakfasts');

    cy.intercept('GET', '/api/tours', {
      body: [
        {
          id: 50,
          title: 'unique_tour',
          commonAmount: 13,
          amountOfGuests: 12,
          description: 'unique_descri',
          price: 1111,
        },
      ],
    }).as('getTours');

    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');

    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');

    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');

    cy.intercept('GET', '/api/guest/110', { id: 110, firstName: 'Name', lastName: 'Surname', rating: null }).as(
      'getGuest'
    );
    cy.intercept('GET', '/api/guests', [{ id: 110, firstName: 'Name', lastName: 'Surname', rating: null }]).as(
      'getGuests'
    );

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 3,
        idGuest: 110,
        idBooking: 103,
      },
    ]).as('getBookingsGuest');
    cy.intercept('GET', '/api/bookings', [
      {
        id: 103,
        arrivalDate: '2023-04-25',
        departureDate: '2023-05-31',
        bill: 427000,
        inCharge: null,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');

    cy.intercept('GET', '/api/booking-rooms/103', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);

    cy.intercept('GET', '/api/booking-breakfast', []);

    cy.intercept('GET', '/api/guest-tour', []);

    cy.intercept('GET', '/api/guest-booking/103', [{ id: 1, guestId: 110, bookingId: 103 }]);
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    //
    cy.get('.addNewBreakfast').click();

    cy.get('[formControlName="menu"]').type('unique_breakfast');
    cy.get('[formControlName="price"]').type('1111');

    cy.get('.addBreakfastSubmit').click();

    cy.wait('@addBreakfast');

    cy.visit('localhost:4200/guest/110');
    cy.get('.changeCurrentBooking').click();
    cy.get('.orderBreakfast').click();

    cy.get('[formControlName="menu"').click().contains('unique_breakfast');
    cy.get('[formControlName="menu"').contains('1111');
  });
});
