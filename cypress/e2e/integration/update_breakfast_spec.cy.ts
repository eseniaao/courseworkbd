describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/2', {
      id: 2,
      firstName: 'Nick',
      lastName: 'Mandich',
      rating: null,
    }).as('getGuest');

    cy.intercept('GET', '/api/guests', [
      {
        id: 2,
        firstName: 'Nick',
        lastName: 'Mandich',
        rating: null,
      },
    ]).as('getGuests');

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 10,
        idGuest: 2,
        idBooking: 2,
      },
    ]).as('getGuestBooking');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');

    cy.intercept('GET', '/api/guest-booking/2', [
      {
        id: 10,
        idGuest: 2,
        idBooking: 2,
      },
    ]).as('getGuestBooking2');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
      },
    ]).as('getBookings');

    cy.intercept('GET', 'api/roomTypes', [
      {
        id: 2,
        type: 'Standart',
        price: 3000,
        guestsAmount: 20,
        typeId: 1,
      },
      {
        id: 3,
        type: 'Villa',
        price: 11000,
        guestsAmount: 20,
        typeId: 2,
      },
    ]).as('getRoomTypes');

    cy.intercept('GET', 'api/booking-rooms/2', [
      {
        id: 2,
        type: 'Standart',
        price: 3000,
        guestsAmount: 20,
        typeId: 1,
      },
    ]).as('getRoomTypes');

    cy.intercept('GET', '/api/tours', []);
    cy.intercept('GET', '/api/booking-breakfast', []);
    cy.intercept('GET', '/api/guest-tour', []);

    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');
    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    });

    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 50, menu: 'unique_breakfast', price: 1111 }],
    }).as('getBreakfasts');

    cy.intercept('PATCH', '/api/breakfast', {
      statusCode: 200,
      body: {
        id: 50,
        menu: 'unique_breakfast2',
        price: 2222,
      },
    }).as('patchBreakfast');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/guest/2');

    cy.get('.changeCurrentBooking').click();
    cy.get('@getBreakfasts');
    cy.get('.orderBreakfast').click();

    cy.get('[formControlName="menu"').click();
    cy.get('[formControlName="menu"').contains('unique_breakfast');
    cy.get('[formControlName="menu"').contains('1111');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('@getBreakfasts');

    cy.contains('unique_breakfast').click().type('{selectall}').type('unique_breakfast2{enter}');
    cy.contains('1111').click().type('{selectall}').type('2222{enter}');

    cy.visit('localhost:4200/guest/2');

    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 50, menu: 'unique_breakfast2', price: 2222 }],
    }).as('getBreakfasts');

    cy.get('.changeCurrentBooking').click();
    cy.get('.orderBreakfast').click();

    cy.get('[formControlName="menu"').click().contains('unique_breakfast2');
    cy.get('[formControlName="menu"').contains('2222');
  });
});
