describe('Тестирование взаимодействия модуля авторизации и главной страницы сотрудника', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/staff', [
      {
        id: 100,
        firstName: 'Santa',
        lastName: 'Lucia',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
      {
        id: 101,
        firstName: 'Lucy2',
        lastName: 'Hops2',
        rating: 4,
        positionId: 10,
        salary: 30000,
      },
      {
        id: 102,
        firstName: 'Tony1',
        lastName: 'Hills1',
        rating: 4,
        positionId: 10,
        salary: 30000,
      },
    ]).as('getStaff');

    cy.intercept('GET', '/api/staff/100', {
      id: 100,
      firstName: 'Santa',
      lastName: 'Lucia',
      rating: 4,
      positionId: 4,
      salary: 42000,
    }).as('getStaff1');

    cy.intercept('GET', '/api/staff/101', {
      id: 101,
      firstName: 'Lucy2',
      lastName: 'Hops2',
      rating: 4,
      positionId: 10,
      salary: 30000,
    }).as('getStaff2');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
      {
        id: 3,
        arrivalDate: '2023-03-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('PATCH', '/api/booking-staff', [
      {
        staffId: 102,
        bookingId: 3,
      },
    ]).as('patchBookingsStaff');

    cy.intercept('GET', '/api/booking-room', [
      {
        id: 1,
        roomId: 1,
        bookingId: 2,
      },
      {
        id: 2,
        roomId: 2,
        bookingId: 3,
      },
    ]).as('getBookingsRoom');

    cy.intercept('GET', '/api/tours', {
      body: [],
    }).as('getPositions');
  });

  it('Авторизация сотрудника и проверка доступности списка комнат', () => {
    cy.visit('localhost:4200/staff');
    cy.intercept('GET', '/api/booking-staff', [
      {
        id: 1,
        staffId: 101,
        bookingId: 2,
      },
      {
        id: 2,
        staffId: 101,
        bookingId: 3,
      },
    ]);

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Lucy2');
    cy.get('[formControlName="lastName"]').type('Hops2');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.contains('Apr 1, 2023');
    cy.contains('Mar 1, 2023');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Santa');
    cy.get('[formControlName="lastName"]').type('Lucia');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.2023-03-01').click().click();
    cy.contains('Hills1').click().type('Cypress.io{enter}');

    cy.wait('@patchBookingsStaff');
    cy.intercept('GET', '/api/booking-staff', [
      {
        id: 1,
        staffId: 101,
        bookingId: 2,
      },
      {
        id: 2,
        staffId: 102,
        bookingId: 3,
      },
    ]);

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Lucy2');
    cy.get('[formControlName="lastName"]').type('Hops2');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.contains('Apr 1, 2023');
    cy.get('Mar 1, 2023').should('not.exist');
  });
});
