describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/2', { id: 2, firstName: 'Nick', lastName: 'Mandich', rating: null }).as('getGuest');
    cy.intercept('GET', '/api/guests', [{ id: 2, firstName: 'Nick', lastName: 'Mandich', rating: null }]).as(
      'getGuests'
    );
    cy.intercept('GET', '/api/guest-booking', []);
    cy.intercept('GET', '/api/bookings', []);
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.visit('localhost:4200/guest');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Nick');
    cy.get('[formControlName="lastName"]').type('Mandich');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем, что страница загрузилась и URL содержит слово manage
    cy.url().should('include', '/2');

    // проверяем наличие элемента редактирования описания
    cy.get('.createNewBooking').should('be.visible').should('not.be.disabled');
  });
});
