describe('Тестирование взаимодействия модуля авторизации и главной страницы сотрудника', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/staff/31', {
      id: 31,
      firstName: 'Santa',
      lastName: 'Lucia',
      rating: 4,
      positionId: 4,
      salary: 42000,
    }).as('getStaff');

    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/staff', [
      {
        id: 31,
        firstName: 'Santa',
        lastName: 'Lucia',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
    ]).as('getStaff2');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
      {
        id: 3,
        arrivalDate: '2023-03-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/booking-staff', [
      {
        id: 1,
        staffId: 101,
        bookingId: 2,
      },
      {
        id: 2,
        staffId: 101,
        bookingId: 3,
      },
    ]).as('getBookingStaff');

    cy.intercept('GET', '/api/booking-room', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);
    cy.intercept('GET', '/api/tours', []).as('getTours');
  });

  it('Авторизация сотрудника и проверка доступности списка комнат', () => {
    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Santa');
    cy.get('[formControlName="lastName"]').type('Lucia');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем, что страница загрузилась и URL содержит слово manage
    cy.url().should('include', '/31');

    // проверяем наличие элемента редактирования описания
    cy.get('app-bookings-table').should('exist');
  });
});
