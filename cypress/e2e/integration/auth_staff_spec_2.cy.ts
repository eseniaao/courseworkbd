describe('Тестирование взаимодействия модуля авторизации и главной страницы сотрудника', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/staff', []);
  });

  it('Авторизация сотрудника и проверка доступности списка комнат', () => {
    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Wrong');
    cy.get('[formControlName="lastName"]').type('Name');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем наличие элемента редактирования описания
    cy.get('.invalidUser').should('exist');
  });
});
