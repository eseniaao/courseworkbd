describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  let interceptCount = 0;
  beforeEach(() => {
    cy.intercept('GET', '/api/tours', {
      body: [
        {
          id: 50,
          title: 'unique_tour',
          commonAmount: 13,
          amountOfGuests: 12,
          description: 'unique_descri',
          price: 1111,
        },
      ],
    }).as('getTours');

    cy.intercept('PATCH', '/api/tour', {
      statusCode: 200,
      body: {
        id: 50,
        title: 'unique_tour2',
        commonAmount: 24,
        amountOfGuests: 23,
        description: 'unique_descri2',
        price: 2222,
      },
    }).as('patchTour');

    cy.intercept('GET', '/api/guest/110', { id: 110, firstName: 'Name', lastName: 'Surname', rating: null }).as(
      'getGuest'
    );
    cy.intercept('GET', '/api/guests', [{ id: 110, firstName: 'Name', lastName: 'Surname', rating: null }]).as(
      'getGuests'
    );

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 3,
        idGuest: 110,
        idBooking: 103,
      },
    ]).as('getBookingsGuest');
    cy.intercept('GET', '/api/bookings', [
      {
        id: 103,
        arrivalDate: '2023-04-25',
        departureDate: '2023-05-31',
        bill: 427000,
        inCharge: null,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');

    cy.intercept('GET', '/api/booking-rooms/103', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);

    cy.intercept('GET', '/api/booking-breakfast', []);

    cy.intercept('GET', '/api/guest-tour', []);

    cy.intercept('GET', '/api/guest-booking/103', [{ id: 1, guestId: 110, bookingId: 103 }]);

    cy.intercept('GET', '/api/breakfasts', []);

    cy.intercept('GET', '/api/positions', []).as('getPositions');

    cy.intercept('GET', '/api/staff', []).as('getStaff');

    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 3, menu: 'fsf', price: 109 }],
    }).as('getBreakfasts');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/guest/110');

    cy.get('.changeCurrentBooking').click();
    cy.get('@getTours');
    cy.get('.orderTour').click();

    cy.get('[formControlName="tour"').click();
    cy.get('[formControlName="tour"').contains('unique_tour');
    cy.get('[formControlName="tour"').contains('1111');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('@getTours');

    cy.contains('unique_tour').click().type('{selectall}').type('unique_tour2{enter}');
    cy.contains('13').click().type('{selectall}').type('24{enter}');
    cy.contains('unique_descri').click().type('{selectall}').type('unique_descri2{enter}');
    cy.contains('1111').click().type('{selectall}').type('2222{enter}');

    cy.visit('localhost:4200/guest/110');

    cy.intercept('GET', '/api/tours', {
      body: [
        {
          id: 50,
          title: 'unique_tour2',
          commonAmount: 24,
          amountOfGuests: 23,
          description: 'unique_descri2',
          price: 2222,
        },
      ],
    }).as('getTours');

    cy.get('.changeCurrentBooking').click();
    cy.get('.orderTour').click();

    cy.get('[formControlName="tour"').click().contains('unique_tour2');
    cy.get('[formControlName="tour"').contains('2222');
  });
});
