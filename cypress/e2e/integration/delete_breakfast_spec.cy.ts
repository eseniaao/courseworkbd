describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  let interceptCount = 0;
  beforeEach(() => {
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 50, menu: 'unique_breakfast', price: 1111 }],
    }).as('getBreakfasts');

    cy.intercept('DELETE', `/api/breakfast`, {
      body: {
        id: 50,
      },
    }).as('deleteBreakfast');
    cy.intercept('GET', '/api/guest/2', { id: 2, firstName: 'Nick', lastName: 'Mandich', rating: null }).as('getGuest');
    cy.intercept('GET', '/api/guests', [{ id: 2, firstName: 'Nick', lastName: 'Mandich', rating: null }]).as(
      'getGuests'
    );
    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-05-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 2,
        idGuest: 2,
        idBooking: 2,
      },
    ]).as('getBookingsGuest');
    cy.intercept('GET', '/api/guest-booking/2', [
      {
        id: 2,
        idGuest: 2,
        idBooking: 2,
      },
    ]);

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');

    cy.intercept('GET', '/api/tours', []);
    cy.intercept('GET', '/api/booking-breakfast', []);
    cy.intercept('GET', '/api/guest-tour', []);

    cy.intercept('GET', '/api/booking-rooms/2', [
      {
        id: 2,
        roomId: 2,
        bookingId: 3,
      },
    ]).as('getBookingsRoom');

    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');
    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/tours', []).as('getTours');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/guest/2');

    cy.get('.changeCurrentBooking').click();
    cy.get('.orderBreakfast').click();

    cy.get('[formControlName="menu"').click();
    cy.get('[formControlName="menu"').contains('unique_breakfast');
    cy.get('[formControlName="menu"').contains('1111');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.deleteBreakfast').click();

    cy.get('.p-confirm-dialog-accept').click();

    cy.wait('@deleteBreakfast');

    cy.visit('localhost:4200/guest/2');

    cy.intercept('GET', '/api/breakfasts', {
      body: [],
    }).as('getBreakfasts');

    cy.get('.changeCurrentBooking').click();
    cy.get('.orderBreakfast').click();

    cy.get('unique_breakfast1').should('not.exist');
  });
});
