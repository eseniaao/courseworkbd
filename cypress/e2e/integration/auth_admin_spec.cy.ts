describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 3, menu: 'fsf', price: 109 }],
    }).as('getBreakfasts');
    cy.intercept('GET', '/api/tours', {}).as('getTours');
    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем, что страница загрузилась и URL содержит слово manage
    cy.url().should('include', '/manage');

    // проверяем наличие элемента редактирования описания
    cy.get('[formControlName="description"]').should('exist');
  });
});
