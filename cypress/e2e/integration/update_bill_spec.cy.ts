describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/2', {
      id: 2,
      firstName: 'Nick',
      lastName: 'Mandich',
      rating: null,
    }).as('getGuest');

    cy.intercept('GET', '/api/guests', [
      {
        id: 2,
        firstName: 'Nick',
        lastName: 'Mandich',
        rating: null,
      },
    ]).as('getGuests');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 10,
        idGuest: 2,
        idBooking: 2,
      },
    ]).as('getGuestBooking');

    cy.intercept('GET', '/api/guest-booking/2', [
      {
        id: 10,
        idGuest: 2,
        idBooking: 2,
      },
    ]).as('getGuestBooking2');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
      },
    ]).as('getBookings');

    cy.intercept('GET', 'api/roomTypes', [
      {
        id: 2,
        type: 'Standart',
        price: 3000,
        guestsAmount: 20,
        typeId: 1,
      },
      {
        id: 3,
        type: 'Villa',
        price: 11000,
        guestsAmount: 20,
        typeId: 2,
      },
    ]).as('getRoomTypes');

    cy.intercept('GET', 'api/booking-rooms/2', [
      {
        id: 2,
        type: 'Standart',
        price: 3000,
        guestsAmount: 20,
        typeId: 1,
      },
    ]).as('getRoomTypes');
    cy.intercept('GET', '/api/breakfasts', []);
    cy.intercept('GET', '/api/tours', []);
    cy.intercept('GET', '/api/booking-breakfast', []);
    cy.intercept('GET', '/api/guest-tour', []);
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.visit('localhost:4200/guest');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Nick');
    cy.get('[formControlName="lastName"]').type('Mandich');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.wait('@getGuestBooking');
    cy.wait('@getBookings');

    // проверяем наличие элемента редактирования описания
    cy.get('.changeCurrentBooking').click();

    cy.get('.addRoom').click();
    cy.contains('Select type').click();
    cy.contains('Standart').click();

    cy.get('.bill').contains('240000');

    cy.get('.addRoom').click();
    cy.contains('Select type').click();
    cy.contains('Villa').click();

    cy.get('.bill').contains('1120000');
  });
});
