describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  let interceptCount = 0;
  beforeEach(() => {
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 50, menu: 'unique_breakfast', price: 1111 }],
    }).as('getBreakfasts');

    cy.intercept('PATCH', '/api/breakfast', {
      statusCode: 200,
      body: {
        id: 50,
        menu: 'unique_breakfast2',
        price: 2222,
      },
    }).as('patchBreakfast');

    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');
    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/tours', []).as('getTours');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('@getBreakfasts');

    cy.contains('unique_breakfast').click().type('{selectall}').type('unique_breakfast2{enter}');
    cy.contains('1111').click().type('{selectall}').type('2222{enter}');

    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 50, menu: 'unique_breakfast2', price: 2222 }],
    }).as('getBreakfasts');
    cy.reload();

    cy.contains('unique_breakfast2');
    cy.contains('2222');
  });
});
