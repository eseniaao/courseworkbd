describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  let interceptCount = 0;
  beforeEach(() => {
    cy.intercept('PATCH', '/api/tour', {
      statusCode: 200,
      body: {
        id: 50,
        title: 'unique_tour2',
      },
    }).as('patchTour1');
    cy.intercept('PATCH', '/api/tour', {
      statusCode: 200,
      body: {
        id: 50,
        commonAmount: 24,
      },
    }).as('patchTour2');
    cy.intercept('PATCH', '/api/tour', {
      statusCode: 200,
      body: {
        id: 50,
        description: 'unique_descri2',
      },
    }).as('patchTour3');
    cy.intercept('PATCH', '/api/tour', {
      statusCode: 200,
      body: {
        id: 50,
        price: 2222,
      },
    }).as('patchTour4');

    cy.intercept('GET', '/api/positions', {
      body: [],
    }).as('getPositions');
    cy.intercept('GET', '/api/staff', {
      body: [],
    }).as('getStaff');
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 3, menu: 'fsf', price: 109 }],
    }).as('getBreakfasts');
    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.visit('http://127.0.0.1:4200/admin');
    cy.intercept('GET', '/api/tours', {
      body: [
        {
          id: 50,
          title: 'unique_tour',
          commonAmount: 13,
          amountOfGuests: 12,
          description: 'unique_descri',
          price: 1111,
        },
      ],
    }).as('getTours');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('@getTours');

    cy.contains('unique_tour').click().type('{selectall}').type('unique_tour2{enter}');
    cy.contains('13').click().type('{selectall}').type('24{enter}');
    cy.contains('unique_descri').click().type('{selectall}').type('unique_descri2{enter}');
    cy.contains('1111').click().type('{selectall}').type('2222{enter}');

    cy.intercept('GET', '/api/tours', {
      body: [
        {
          id: 50,
          title: 'unique_tour2',
          commonAmount: 24,
          amountOfGuests: 23,
          description: 'unique_descri2',
          price: 2222,
        },
      ],
    }).as('getTours');
    cy.reload();

    cy.get('@getTours');

    cy.contains('unique_tour2');
    cy.contains('24');
    cy.contains('unique_descri2');
    cy.contains('2222');
  });
});
