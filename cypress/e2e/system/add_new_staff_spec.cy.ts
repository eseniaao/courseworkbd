describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('POST', `/api/staff`, {
      body: {
        id: 105,
        firstName: 'Test',
        lastName: 'Staff',
        rating: 4,
        positionId: 4,
        salary: 40000,
      },
    }).as('postStaff');

    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 3, menu: 'fsf', price: 109 }],
    }).as('getBreakfasts');
    cy.intercept('GET', '/api/tours', []).as('getTours');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
      {
        id: 3,
        arrivalDate: '2023-03-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/booking-staff', [
      {
        id: 1,
        staffId: 101,
        bookingId: 2,
      },
      {
        id: 2,
        staffId: 101,
        bookingId: 3,
      },
    ]).as('getBookingStaff');

    cy.intercept('GET', '/api/booking-room', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);
    cy.intercept('GET', '/api/tours', []).as('getTours');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.intercept('GET', '/api/staff', []).as('getStaff');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем наличие элемента редактирования описания
    cy.get('.addNewStaff').click();
    cy.get('[formControlName="firstName"]').type('Test');
    cy.get('[formControlName="lastName"]').type('Staff');
    cy.contains('Select position').click();
    cy.contains('Cleaning Manager').click();

    cy.get('.addNewStaffButton').click();

    cy.wait('@postStaff');

    cy.intercept('GET', '/api/staff', [
      {
        id: 105,
        firstName: 'Test',
        lastName: 'Staff',
        rating: 4,
        positionId: 4,
        salary: 40000,
      },
    ]).as('getStaff');

    cy.intercept('GET', '/api/staff/105', {
      id: 105,
      firstName: 'Test',
      lastName: 'Staff',
      rating: 4,
      positionId: 4,
      salary: 40000,
    }).as('getStaff2');

    cy.contains('40000');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Test');
    cy.get('[formControlName="lastName"]').type('Staff');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('app-bookings-table').should('exist');
  });
});
