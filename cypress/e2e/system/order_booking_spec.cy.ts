describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/110', { id: 110, firstName: 'Name', lastName: 'Surname', rating: null }).as(
      'getGuest'
    );
    cy.intercept('GET', '/api/guests', [{ id: 110, firstName: 'Name', lastName: 'Surname', rating: null }]).as(
      'getGuests'
    );

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 2,
        idGuest: 110,
        idBooking: 2,
      },
    ]).as('getBookingsGuest');
    cy.intercept('GET', '/api/guest-booking/2', [
      {
        id: 2,
        idGuest: 110,
        idBooking: 2,
      },
    ]).as('getBookingsGuest2');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-05-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');

    cy.intercept('GET', '/api/booking-rooms/2', [
      { id: 1, roomId: 1, bookingId: 2 },
      { id: 2, roomId: 2, bookingId: 2 },
    ]).as('getRoomsBooking');

    cy.intercept('GET', '/api/breakfasts', [
      { id: 1, menu: 'first', price: 123123 },
      { id: 2, menu: 'second', price: 1000 },
    ]);
    cy.intercept('GET', '/api/tours', {});
    cy.intercept('GET', '/api/guest-tour', {});

    cy.intercept('POST', '/api/booking-breakfast', {
      statusCode: 200,
      body: {
        id: 1,
        breakfastId: 1,
        bookingId: 2,
      },
    });
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.visit('localhost:4200/guest');
    cy.intercept('GET', '/api/booking-breakfast', {});

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Name');
    cy.get('[formControlName="lastName"]').type('Surname');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.changeCurrentBooking').click();
    cy.contains('Order breakfast').click();
    cy.contains('Select breakfast').click();
    cy.contains('first').click();
    cy.get('.submitBreakfast').click();

    cy.intercept('GET', '/api/booking-breakfast', { id: 1, breakfastId: 1, bookingId: 2 });

    cy.contains('123123');
  });
});
