describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/110', { id: 110, firstName: 'Name', lastName: 'Surname', rating: null }).as(
      'getGuest'
    );
    cy.intercept('GET', '/api/guests', [{ id: 110, firstName: 'Name', lastName: 'Surname', rating: null }]).as(
      'getGuests'
    );

    cy.intercept('POST', '/api/booking', {
      body: {
        id: 103,
        arrivalDate: '2023-04-25',
        departureDate: '2023-05-31',
        bill: 427000,
        inCharge: null,
      },
    }).as('postBooking');

    cy.intercept('POST', '/api/booking-guest', [
      {
        body: {
          id: 3,
          idGuest: 110,
          idBooking: 103,
        },
      },
    ]).as('postBookingGuest');

    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 2,
        idGuest: 110,
        idBooking: 2,
      },
    ]).as('getBookingsGuest');

    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-02-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.visit('localhost:4200/guest');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Name');
    cy.get('[formControlName="lastName"]').type('Surname');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.changeCurrentBooking').should('not.exist');

    cy.get('.createNewBooking').click();
    cy.get('[formControlName="arrivalDate"]').click();
    cy.contains('25').click();
    cy.get('[formControlName="departureDate"]').click();
    cy.get('.p-datepicker-next').click();
    cy.contains('31').click();

    cy.get('.addRoom').click();
    cy.contains('Select type').click();
    cy.contains('One-bed').click();

    cy.get('button[type="submit"]').click();

    cy.wait('@postBooking');
    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-02-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
      {
        id: 103,
        arrivalDate: '2023-04-25',
        departureDate: '2023-05-31',
        bill: 427000,
        inCharge: null,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/guest-booking', [
      {
        id: 2,
        idGuest: 110,
        idBooking: 2,
      },
      {
        id: 3,
        idGuest: 110,
        idBooking: 103,
      },
    ]).as('getBookingsGuest');

    cy.reload();

    cy.get('.changeCurrentBooking').should('exist');
  });
});
