describe('Тестирование взаимодействия модуля авторизации и главной страницы администрации', () => {
  beforeEach(() => {
    cy.intercept('PATCH', `/api/staff`, {
      body: {
        id: 102,
        firstName: 'Some',
        lastName: 'Mad',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
    }).as('patchStaff');

    cy.intercept('GET', '/api/positions', [
      { id: 4, basicSalary: 40000, name: 'Cleaning Manager' },
      { id: 10, basicSalary: 30000, name: 'Maid' },
    ]).as('getPositions');

    cy.intercept('GET', '/api/booking-room', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);
    cy.intercept('GET', '/api/tours', []).as('getTours');
    cy.intercept('GET', '/api/bookings', [
      {
        id: 2,
        arrivalDate: '2023-04-01',
        departureDate: '2023-06-20',
        bill: 427000,
        inCharge: 101,
      },
      {
        id: 3,
        arrivalDate: '2023-03-01',
        departureDate: '2023-03-20',
        bill: 427000,
        inCharge: 101,
      },
    ]).as('getBookings');

    cy.intercept('GET', '/api/booking-staff', [
      {
        id: 1,
        staffId: 101,
        bookingId: 2,
      },
      {
        id: 2,
        staffId: 101,
        bookingId: 3,
      },
    ]).as('getBookingStaff');

    cy.intercept('GET', '/api/description', {
      body: [{ description: 'goodbye' }],
    }).as('getDesc');
    cy.intercept('GET', '/api/breakfasts', {
      body: [{ id: 3, menu: 'fsf', price: 109 }],
    }).as('getBreakfasts');
  });

  it('Авторизация администратора и проверка доступности списка сотрудников и услуг', () => {
    cy.intercept('GET', '/api/staff', [
      {
        id: 102,
        firstName: 'Some',
        lastName: 'Mad',
        rating: 4,
        positionId: 10,
        salary: 42000,
      },
      {
        id: 101,
        firstName: 'Lucy2',
        lastName: 'Hops2',
        rating: 4,
        positionId: 10,
        salary: 30000,
      },
    ]).as('getStaffs');

    cy.intercept('GET', '/api/staff/102', {
      id: 102,
      firstName: 'Some',
      lastName: 'Mad',
      rating: 4,
      positionId: 10,
      salary: 42000,
    }).as('getStaff');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Some');
    cy.get('[formControlName="lastName"]').type('Mad');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.bookingsRecord').should('not.exist');

    cy.visit('localhost:4200/admin');

    // вводим пароль
    cy.get('input[type="password"][formControlName="password"]').type('secret');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.contains('Maid').click();
    cy.contains('Select position').click();
    cy.contains('Cleaning Manager').click().type('{enter}');

    cy.wait('@patchStaff');
    cy.intercept('GET', '/api/staff', [
      {
        id: 102,
        firstName: 'Some',
        lastName: 'Mad',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
      {
        id: 101,
        firstName: 'Lucy2',
        lastName: 'Hops2',
        rating: 4,
        positionId: 10,
        salary: 30000,
      },
    ]).as('getStaffs');

    cy.intercept('GET', '/api/staff/102', {
      id: 102,
      firstName: 'Some',
      lastName: 'Mad',
      rating: 4,
      positionId: 4,
      salary: 42000,
    }).as('getStaff');

    cy.visit('localhost:4200/staff');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Some');
    cy.get('[formControlName="lastName"]').type('Mad');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    cy.get('.bookingRecord').should('exist');
  });
});
