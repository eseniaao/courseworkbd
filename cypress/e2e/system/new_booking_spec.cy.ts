describe('Тестирование взаимодействия модуля авторизации и главной страницы гостя', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/guest/94', { id: 94, firstName: 'Test', lastName: 'User', rating: null }).as('getGuest');
    cy.intercept('GET', '/api/guests', [{ id: 94, firstName: 'Test', lastName: 'User', rating: null }]).as('getGuests');
    cy.intercept('GET', '/api/guest-booking', [{ id: 1, idGuest: 94, idBooking: 103 }]).as('getGuestBooking');
    cy.intercept('GET', '/api/tours', []).as('getTours');
    cy.intercept('GET', '/api/booking-staff', [
      { id: 1, staffId: 1, bookingId: 103 },
      { id: 2, staffId: 2, bookingId: 103 },
    ]);
    cy.intercept('GET', '/api/booking-room', [
      { id: 1, roomId: 1, bookingId: 103 },
      { id: 2, roomId: 2, bookingId: 103 },
    ]);
    cy.intercept('GET', '/api/rooms', [
      { id: 1, typeId: 6 },
      { id: 2, typeId: 6 },
    ]).as('getRooms');
    cy.intercept('GET', '/api/roomTypes', [
      { id: 1, type: 'Standart', price: 3000, roomsAmount: 10, guestsAmount: 2 },
      { id: 6, type: 'One-bed Standart', price: 2000, roomsAmount: 4, guestsAmount: 1 },
    ]).as('getRoomTypes');
    cy.intercept('GET', '/api/positions', [{ id: 4, basicSalary: 40000, name: 'Cleaning Manager' }]);
    cy.intercept('GET', '/api/staff/100', {
      id: 100,
      firstName: 'Santa',
      lastName: 'Lucia',
      rating: 4,
      positionId: 4,
      salary: 42000,
    }).as('getStaff1');
    cy.intercept('GET', '/api/staff', [
      {
        id: 100,
        firstName: 'Santa',
        lastName: 'Lucia',
        rating: 4,
        positionId: 4,
        salary: 42000,
      },
    ]).as('getStaff');
    cy.intercept('GET', '/api/bookings', [
      {
        id: 103,
        arrivalDate: '2023-04-25',
        departureDate: '2023-05-31',
        bill: 427000,
        inCharge: null,
      },
    ]).as('getBookings');
  });

  it('Авторизация гостя и проверка доступности кнопок управления бронированиями', () => {
    cy.visit('localhost:4200/guest');

    cy.wait('@getGuests');

    // вводим данные
    cy.get('[formControlName="firstName"]').type('Test');
    cy.get('[formControlName="lastName"]').type('User');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем, что страница загрузилась и URL содержит слово manage
    cy.url().should('include', '/94');

    // проверяем наличие элемента редактирования описания
    cy.get('.createNewBooking').click();
    cy.get('[formControlName="arrivalDate"]').click();
    cy.contains('25').click();
    cy.get('[formControlName="departureDate"]').click();
    cy.get('.p-datepicker-next').click();
    cy.contains('31').click();

    cy.get('.addRoom').click();
    cy.contains('Select type').click();
    cy.contains('One-bed').click();

    cy.get('.addGuest').click();
    cy.get('[formControlName="firstName"]').type('Test2');
    cy.get('[formControlName="lastName"]').type('User2');

    cy.get('button[type="submit"]').click();

    cy.get('.message').should('be.visible');

    cy.get('.addRoom').click();
    cy.contains('Select type').click();
    cy.contains('One-bed').click();

    cy.get('button[type="submit"]').click();
    cy.get('.message').should('not.be.visible');

    cy.visit('localhost:4200/staff');

    cy.get('[formControlName="firstName"]').type('Santa');
    cy.get('[formControlName="lastName"]').type('Lucia');

    // кликаем на кнопку входа
    cy.get('button[type="submit"]').click();

    // проверяем наличие элемента редактирования описания
    cy.contains('May 31, 2023').should('exist');
  });
});
