import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrderComponent } from './create-order.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateOrderModule } from './create-order.module';
import {
  Booking,
  Booking_Breakfast,
  Breakfast,
  Guest,
  Guest_Booking,
  Guest_Tour,
  Room,
  Staff,
  Tour,
} from '../../../shared/interfaces';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockGuest: Guest = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  bookings: [mockBooking],
};

const mockGuestBooking: Guest_Booking = {
  id: 1,
  idGuest: 1,
  idBooking: 1,
};

const mockGuestBooking2: Guest_Booking = {
  id: 1,
  idGuest: 2,
  idBooking: 1,
};

const mockTour: Tour = {
  id: 1,
  title: 'tour',
  commonAmount: 2,
  amountOfGuests: 0,
  description: 'description',
  price: 123,
};

const mockTour2: Tour = {
  id: 2,
  title: 'tour2',
  commonAmount: 2,
  amountOfGuests: 0,
  description: 'description',
  price: 124,
};

const mockGuestTour: Guest_Tour = {
  id: 1,
  guestId: 1,
  tourId: 2,
};

const mockBreakfast: Breakfast = {
  id: 1,
  menu: 'menu1',
  price: 100,
};

const mockBreakfast1: Breakfast = {
  id: 2,
  menu: 'menu2',
  price: 200,
};

const mockBreakfastBooking: Booking_Breakfast = {
  id: 1,
  breakfastId: 1,
  bookingId: 1,
};

const mockBreakfastBooking2: Booking_Breakfast = {
  id: 2,
  breakfastId: 1,
  bookingId: 2,
};

describe('CreateOrderComponent', () => {
  let component: CreateOrderComponent;
  let fixture: ComponentFixture<CreateOrderComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateOrderModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrderComponent);
    component = fixture.componentInstance;
    component.curBooking = mockBooking;
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('invoke method "submitBreakfast"', () => {
    const [menu] = [{ name: 'menu', id: 1 }];

    it('should return undefined if form is empty', () => {
      // Act
      component.submitBreakfast();

      // Assert
      httpMock.expectNone({ method: 'post', url: '/api/booking-breakfast' });
      expect(true).toBeTruthy();
    });

    it('should invoke http request for creating new breakfast-booking if orderBreakfastForm is valid', () => {
      // Arrange
      component.orderBreakfastForm.patchValue({
        menu,
      });

      // Act
      component.submitBreakfast();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/booking-breakfast' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ bookingId: component.curBooking!.id, breakfastId: menu.id });
    });
  });

  describe('invoke method "submitTour"', () => {
    const [guestId, tourId] = [1, mockTour.id];

    it('should return undefined if form is empty', () => {
      // Act
      component.submitTour();

      // Assert
      httpMock.expectNone({ method: 'post', url: '/api/guest-tour' });
      expect(true).toBeTruthy();
    });

    it('should invoke http request  for creating new guest-tour if orderTourForm is valid', () => {
      // Arrange
      component.orderTourForm.patchValue({
        tour: tourId,
        guests: guestId,
      });

      // Act
      component.submitTour();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/guest-tour' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ guestId: undefined, tourId: undefined });
    });
  });

  describe('method "initialize"', () => {
    it('should invoke get http requests for inner parameters', () => {
      // Arrange
      component.curBooking = mockBooking;

      spyOn(httpClient, 'get')
        .withArgs('/api/breakfasts')
        .and.returnValue(of([]))
        .withArgs('/api/tours')
        .and.returnValue(of([]))
        .withArgs(`/api/guest-booking/${component.curBooking!.id}`)
        .and.returnValue(of([]))
        .withArgs('/api/booking-breakfast')
        .and.returnValue(of([]))
        .withArgs('/api/guest-tour')
        .and.returnValue(of([]))
        .withArgs('/api/guests')
        .and.returnValue(of([]));

      // Act
      component.initialize();

      // Assert
      expect(httpMock.expectOne({ method: 'get', url: '/api/breakfasts' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/tours' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/booking-breakfast' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/guest-tour' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: '/api/guests' })).toBeTruthy();
    });
  });

  it('method "formatGuests" should return only guests for current booking', () => {
    // Arrange
    const guests = [mockGuest];
    const guest_bookings = [mockGuestBooking2, mockGuestBooking];

    // Act
    component.formatGuests(guest_bookings, guests);

    // Assert
    expect(component.commonGuests).toEqual([mockGuest]);
  });

  it('method "formatBreakfasts" should return breakfasts for current booking', () => {
    // Arrange
    component.curBooking = mockBooking;
    const breakfasts = [mockBreakfast, mockBreakfast1];
    const breakfast_bookings = [mockBreakfastBooking, mockBreakfastBooking2];

    // Act
    const result = component.formatBreakfasts(breakfasts, breakfast_bookings);

    // Assert
    expect(result).toEqual([mockBreakfast]);
  });

  it('method "formatTours" should return tours for current booking', () => {
    // Arrange
    const tours = [mockTour, mockTour2];
    const guest_tours = [mockGuestTour];
    const guest_bookings = [mockGuestBooking, mockGuestBooking2];
    // Act
    component.formatTours(tours, guest_bookings, guest_tours);

    // Assert
    expect(component.orderedTours).toEqual([{ id: mockGuestTour.id, title: mockTour2.title, price: mockTour2.price }]);
  });
});
