import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestAuthPageComponent } from './guest-auth-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GuestAuthPageModule } from './guest-auth-page.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Booking, Guest, Guest_Booking } from '../../../shared/interfaces';
import { Router } from '@angular/router';
import { of } from 'rxjs';

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockGuest: Guest = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  bookings: [mockBooking],
};

const mockGuest2: Guest = {
  id: 2,
  firstName: 'firstName',
  lastName: 'lastName',
  bookings: [],
};

const mockGuestBooking: Guest_Booking = {
  id: 1,
  idGuest: 2,
  idBooking: 1,
};

describe('GuestAuthPageComponent', () => {
  let component: GuestAuthPageComponent;
  let fixture: ComponentFixture<GuestAuthPageComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GuestAuthPageModule, HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestAuthPageComponent);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    router = TestBed.inject(Router);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('method "initialize" should invoke get http requests for guests, bookings and its connections', () => {
    // Arrange
    spyOn(httpClient, 'get')
      .withArgs('api/guests')
      .and.returnValue(of([mockGuest2]))
      .withArgs('api/bookings')
      .and.returnValue(of([mockBooking]))
      .withArgs('api/guest-booking')
      .and.returnValue(of([mockGuestBooking]));

    // Act
    component.initialize();

    // Assert
    expect(httpMock.expectOne({ method: 'get', url: 'api/guests' })).toBeTruthy();
    expect(httpMock.expectOne({ method: 'get', url: 'api/bookings' })).toBeTruthy();
    expect(httpMock.expectOne({ method: 'get', url: 'api/guest-booking' })).toBeTruthy();

    expect(component.guests).toEqual([
      { id: 2, firstName: 'firstName', lastName: 'lastName', bookings: [mockBooking] },
    ]);
  });

  describe('method "Submit"', () => {
    it('should route to guests profile if guests exists', () => {
      // Arrange
      const routerSpy = spyOn(router, 'navigate');
      component.guests.push(mockGuest);

      component.form.patchValue({
        firstName: mockGuest.firstName,
        lastName: mockGuest.lastName,
      });

      // Act
      component.submit();

      // Assert
      expect(component.showGuestProfile).toBe(true);
      expect(routerSpy).toHaveBeenCalledWith(['guest', mockGuest.id]);
    });

    it('should invoke post http request to create new guest if guest does not exist', () => {
      // Arrange
      component.form.patchValue({
        firstName: mockGuest.firstName,
        lastName: mockGuest.lastName,
      });

      // Act
      component.submit();

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/guests' });

      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ firstName: mockGuest.firstName, lastName: mockGuest.lastName });
    });
  });

  it('method "formatGuests" should return guests array with proper bookings', () => {
    // Arrange
    const guests = [mockGuest2];
    const guest_bookings = [mockGuestBooking];
    const bookings = [mockBooking];

    // Act
    const result = component.formatGuests([guests, guest_bookings, bookings]);

    // Assert
    expect(result).toEqual([{ id: 2, firstName: 'firstName', lastName: 'lastName', bookings: [mockBooking] }]);
  });
});
