import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingFormComponent } from './booking-form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BookingFormModule } from './booking-form.module';
import { Booking, Booking_Room, Guest, Guest_Booking, Room, RoomType } from '../../../shared/interfaces';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { compileComponentFromMetadata } from '@angular/compiler';

const mockRoom: RoomType = {
  id: 1,
  type: 'type1',
  price: 3000,
  roomsAmount: 2,
  guestsAmount: 1,
};

const mockRoom3: Room = {
  id: 1,
  typeId: 2,
  type: 'type3',
};

const mockRoom2: RoomType = {
  id: 2,
  type: 'type2',
  price: 4000,
  roomsAmount: 3,
  guestsAmount: 2,
};

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockBookingRoom: Booking_Room = {
  id: 1,
  bookingId: 1,
  roomId: 1,
};

const mockGuest: Guest = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  bookings: [mockBooking],
};

const mockGuest2: Guest = {
  id: 2,
  firstName: 'firstName2',
  lastName: 'lastName2',
  bookings: [mockBooking],
};

const mockGuest3: Guest = {
  id: 3,
  firstName: 'firstName3',
  lastName: 'lastName3',
  bookings: [mockBooking],
};

const mockGuestBooking: Guest_Booking = {
  id: 1,
  idGuest: 1,
  idBooking: 1,
};

describe('BookingFormComponent', () => {
  let component: BookingFormComponent;
  let fixture: ComponentFixture<BookingFormComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BookingFormModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingFormComponent);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('method "initialize"', () => {
    it('should call setNewBookingData() if there is no existing booking', () => {
      // Arrange
      spyOn(component, 'setNewBookingData').and.callThrough();

      // Act
      component.initialize();

      // Assert
      expect(component.setNewBookingData).toHaveBeenCalled();
    });

    it('should call setBookingData() if there is existing booking', () => {
      // Arrange
      spyOn(component, 'setBookingData').and.callThrough();
      component.booking = mockBooking;
      // Act
      component.initialize();

      // Assert
      expect(component.setBookingData).toHaveBeenCalled();
    });
  });

  it('method "areRoomsEnough"', () => {
    // Arrange
    component.form.value.rooms = [mockRoom, mockRoom2];
    component.form.value.guests = [mockGuest, mockGuest2];
    // Act
    const result = component.areRoomsEnough();

    // Assert
    expect(result).toBe(true);
  });

  describe('method "getBill"', () => {
    it('show error in case arrival date is after departure date', () => {
      // Arrange
      const arrivalDate = new Date('10-10-2010');
      const departureDate = new Date('9-9-2010');

      component.form.patchValue({
        arrivalDate: arrivalDate,
        departureDate: departureDate,
      });

      // Act
      component.getBill();

      // Assert
      expect(component.msgs).toEqual([{ severity: 'error', summary: 'Departure date must be after the Arrival date' }]);
    });
    it('get correct bill if arrival and departure date are valid', () => {
      // Arrange
      const arrivalDate = new Date('10-10-2010');
      const departureDate = new Date('11-11-2010');

      component.form.patchValue({
        arrivalDate: arrivalDate,
        departureDate: departureDate,
      });

      // Act
      component.getBill();

      // Assert
      expect(component.msgs).toEqual([]);
    });
  });

  describe('method "submit"', () => {
    const arrivalDate = new Date('10-10-2010');
    const departureDate = new Date('11-11-2010');

    it('should return undefined if form is invalid', () => {
      // Act
      const result = component.submit();

      // Assert
      expect(result).toBe(undefined);
    });

    it('should get message error if form is valid and rooms aren`t enough', () => {
      // Arrange
      component.form.patchValue({
        arrivalDate: arrivalDate,
        departureDate: departureDate,
      });

      component.form.value.rooms = [mockRoom];
      component.form.value.guests = [mockGuest, mockGuest2];

      // Act
      component.submit();

      // Assert
      expect(component.msgs).toEqual([
        { severity: 'error', summary: 'Not enough rooms chosen for this amount of guests' },
      ]);
    });

    it('shouldn`t get message error if form is valid and rooms are enough', () => {
      // Arrange
      component.form.patchValue({
        arrivalDate: arrivalDate,
        departureDate: departureDate,
      });

      [mockRoom2].forEach((value) => {
        (component.form.controls['rooms'] as unknown as AbstractControl[]).push(new FormControl(value));
      });

      [mockGuest].forEach((value) => {
        (component.form.controls['guests'] as unknown as AbstractControl[]).push(new FormControl(value));
      });

      // Act
      component.submit();

      // Assert
      expect(component.msgs).toEqual([]);
    });

    it('should invoke post http request to create new booking if form is valid and rooms are enough', () => {
      // Arrange
      component.form.patchValue({
        arrivalDate,
        departureDate,
      });

      [mockRoom, mockRoom2].forEach((value) => {
        (component.form.controls['rooms'] as unknown as AbstractControl[]).push(new FormControl(value));
      });

      [mockGuest, mockGuest2].forEach((value) => {
        (component.form.controls['guests'] as unknown as AbstractControl[]).push(new FormControl(value));
      });

      const requestsSpy = spyOn(httpClient, 'post')
        .withArgs('/api/booking', component.getFormBooking())
        .and.returnValue(of(mockBooking))
        .withArgs('/api/guests', { firstName: 'firstName', lastName: 'lastName' })
        .and.returnValue(of(mockGuest))
        .withArgs('/api/guests', { firstName: 'firstName2', lastName: 'lastName2' })
        .and.returnValue(of(mockGuest2))
        .withArgs('/api/booking-guest', { idGuest: 1, idBooking: 1 })
        .and.returnValue(of({ id: 1, idGuest: 1, idBooking: 1 }))
        .withArgs('/api/booking-guest', { idGuest: 2, idBooking: 1 })
        .and.returnValue(of({ id: 2, idGuest: 2, idBooking: 1 }))
        .withArgs('/api/booking-guest', { idGuest: 3, idBooking: 1 })
        .and.returnValue(of({ id: 3, idGuest: 3, idBooking: 1 }));

      spyOn(component, 'addBookingGuest').and.callThrough();
      component.guest = mockGuest3;
      // Act
      component.submit();

      // Assert
      expect(requestsSpy).toHaveBeenCalledWith('/api/booking', component.getFormBooking());
      expect(requestsSpy).toHaveBeenCalledWith('/api/guests', { firstName: 'firstName', lastName: 'lastName' });
      expect(requestsSpy).toHaveBeenCalledWith('/api/guests', { firstName: 'firstName2', lastName: 'lastName2' });
      expect(requestsSpy).toHaveBeenCalledWith('/api/booking-guest', { idGuest: 1, idBooking: 1 });
      expect(requestsSpy).toHaveBeenCalledWith('/api/booking-guest', { idGuest: 2, idBooking: 1 });
      expect(component.addBookingGuest).toHaveBeenCalledWith(mockGuest3, mockBooking);
    });
  });

  it('method "addGuest"', () => {
    // Arrange
    const group = component.form.get('guests') as FormArray;
    let groupSize = group.length;
    // Act
    component.addGuest();

    // Assert
    expect(group.length).toEqual(groupSize + 1);
  });

  describe('method "addRoom"', () => {
    it('should add empty control if there is no value', () => {
      // Arrange
      const group = component.form.get('rooms') as FormArray;
      let groupSize = group.length;
      // Act
      component.addRoom();

      // Assert
      expect(group.length).toEqual(groupSize + 1);
    });

    it('should create new control and fill it with value', () => {
      // Arrange
      const group = component.form.get('rooms') as FormArray;
      let groupSize = group.length;

      // Act
      component.addRoom({ type: 'type', price: 120, guestsAmount: 2, typeId: 1 });

      // Assert
      expect(group.length).toEqual(groupSize + 1);
      expect(group.controls[0].value).toEqual({ type: 'type', price: 120, guestsAmount: 2, typeId: 1 });
    });
  });

  it('method "deleteRoom"', () => {
    // Arrange
    const group = component.form.get('rooms') as FormArray;
    const formControl = new FormControl('', Validators.required);

    // Act
    component.deleteRoom(formControl);

    // Assert
    expect(group.controls.includes(formControl)).toBe(false);
  });

  it('method "deleteGuest"', () => {
    // Arrange
    const group = component.form.get('guests') as FormArray;
    const formGroup = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
    });

    // Act
    component.deleteGuest(formGroup);

    // Assert
    expect(group.controls.includes(formGroup)).toBe(false);
  });

  describe('method "setBookingData"', () => {
    it('should return void if there`s no booking)', () => {
      // Arrange
      component.booking = undefined;

      // Act
      component.setBookingData();

      // Assert
      httpMock.expectNone({ method: 'get', url: '/api/rooms' });
      expect(true).toBeTruthy();
    });

    it('should invoke get http requests for roomTypes, bookingRooms, bookingGuests, rooms and guests', () => {
      // Arrange
      component.booking = mockBooking;
      component.guest = mockGuest3;

      spyOn(httpClient, 'get')
        .withArgs('api/rooms')
        .and.returnValue(of([mockRoom3]))
        .withArgs('api/roomTypes')
        .and.returnValue(of([mockRoom2]))
        .withArgs(`api/booking-rooms/${component.booking.id}`)
        .and.returnValue(of([mockBookingRoom]))
        .withArgs(`api/guest-booking/${component.booking.id}`)
        .and.returnValue(of([mockGuestBooking]))
        .withArgs('api/guests')
        .and.returnValue(of([mockGuest]));

      spyOn(component, 'addRoom').and.callThrough();
      spyOn(component, 'addGuest').and.callThrough();
      // Act
      component.setBookingData();

      // Assert
      expect(httpMock.expectOne({ method: 'get', url: 'api/rooms' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: 'api/roomTypes' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: 'api/guests' })).toBeTruthy();

      expect(component.bookingRooms).toEqual([mockBookingRoom]);
      expect(component.bookingGuests).toEqual([mockGuestBooking]);
      expect(component.freeRooms).toEqual([mockRoom3]);
      expect(component.allGuests).toEqual([mockGuest]);

      expect(component.addRoom).toHaveBeenCalled();
      expect(component.addGuest).toHaveBeenCalled();
    });
  });

  describe('method "setNewBookingData"', () => {
    it('should set information got from http requests', () => {
      // Arrange
      spyOn(httpClient, 'get')
        .withArgs('api/rooms')
        .and.returnValue(of([mockRoom3]))
        .withArgs('api/roomTypes')
        .and.returnValue(of([mockRoom]))
        .withArgs('api/guests')
        .and.returnValue(of([mockGuest, mockGuest2]));

      // Act
      component.setNewBookingData();

      // Assert
      expect(httpMock.expectOne({ method: 'get', url: 'api/rooms' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: 'api/roomTypes' })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: 'api/guests' })).toBeTruthy();

      expect(component.freeRooms).toEqual([mockRoom3]);
      expect(component.roomTypes).toEqual([mockRoom]);
      expect(component.allGuests).toEqual([mockGuest, mockGuest2]);
    });
  });
});
