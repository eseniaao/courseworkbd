import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestProfileComponent } from './guest-profile.component';
import { GuestProfileModule } from './guest-profile.module';
import { RouterTestingModule } from '@angular/router/testing';
import { Booking, Guest, Guest_Booking } from '../../../shared/interfaces';
import { of } from 'rxjs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockGuest: Guest = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  bookings: [],
};

const mockGuestBooking: Guest_Booking = {
  id: 1,
  idGuest: 1,
  idBooking: 1,
};

describe('GuestProfileComponent', () => {
  let component: GuestProfileComponent;
  let fixture: ComponentFixture<GuestProfileComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GuestProfileModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestProfileComponent);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('method "formatGuest" should return guest with its bookings array', () => {
    // Arrange
    const bookings = [mockBooking];
    const guest_bookings = [mockGuestBooking];

    // Act
    const result = component.formatGuest([mockGuest, guest_bookings, bookings]);

    // Assert
    expect(result).toEqual({ id: 1, firstName: 'firstName', lastName: 'lastName', bookings: [mockBooking] });
  });

  describe('method "initialize"', () => {
    it('should invoke get http request for guest by its id', () => {
      // Arrange
      const id = '1';

      const httpSpy = spyOn(httpClient, 'get')
        .withArgs(`/api/guest/${id}`)
        .and.returnValue(of([mockGuest]))
        .withArgs('api/bookings')
        .and.returnValue(of([mockBooking]))
        .withArgs('api/guest-booking')
        .and.returnValue(of([mockGuestBooking]));

      // Act
      component.initialize(id);

      // Assert
      expect(httpSpy).toHaveBeenCalledWith(`/api/guest/${id}`);
    });
  });
});
