import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SECRET_PASSWORD } from '../../../app.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-auth-page',
  templateUrl: './admin-auth-page.component.html',
  styleUrls: ['./admin-auth-page.component.css'],
})
export class AdminAuthPageComponent implements OnInit {
  showManagePage = false;
  invalidPassword = false;

  form = this.fb.group({
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private router: Router) {}

  ngOnInit(): void {}

  submit() {
    if (!this.form.valid) return;

    if (this.form.value.password === SECRET_PASSWORD) return this.router.navigate(['admin/manage']);

    this.invalidPassword = true;

    return;
  }
}
