import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAuthPageComponent } from './admin-auth-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AdminAuthPageModule } from './admin-auth-page.module';
import { Router } from '@angular/router';
import { SECRET_PASSWORD } from '../../../app.component';

describe('AdminAuthPageComponent', () => {
  let component: AdminAuthPageComponent;
  let fixture: ComponentFixture<AdminAuthPageComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, AdminAuthPageModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAuthPageComponent);
    router = TestBed.inject(Router);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('invoke method "submit"', () => {
    it('should return undefined if form is empty', async () => {
      // Act
      const result = await component.submit();

      // Assert
      expect(result).toBe(undefined);
    });

    it('should invoke router navigation if password is correct', async () => {
      // Arrange
      const routerSpy = spyOn(router, 'navigate');
      component.form.setValue({ password: SECRET_PASSWORD });

      // Act
      await component.submit();

      // Assert
      expect(routerSpy).toHaveBeenCalledWith(['admin/manage']);
    });

    it('shouldn`t invoke router navigation if password is incorrect and set invalidPassword property to true', async () => {
      // Arrange
      const routerSpy = spyOn(router, 'navigate');
      component.form.setValue({ password: 'some incorrect password' });

      // Act
      await component.submit();

      // Assert
      expect(routerSpy).not.toHaveBeenCalled();
      expect(component.invalidPassword).toBe(true);
    });
  });
});
