import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, map } from 'rxjs';
import { Booking, Booking_Room, Breakfast, Room, Staff, Staff_Booking, Tour } from '../../../shared/interfaces';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-staff-manage',
  templateUrl: './staff-manage.component.html',
  styleUrls: ['./staff-manage.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StaffManageComponent implements OnInit {
  person: Staff | undefined;

  staff: Staff[] = [];
  tours: Tour[] = [];
  bookings: Booking[] = [];

  constructor(private cRef: ChangeDetectorRef, private httpClient: HttpClient, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.pipe(map((p) => p.get('id'))).subscribe(() => {
      const id = this.route.snapshot.paramMap.get('id');

      if (id) this.initialize(id);
    });
  }

  initialize(id: string) {
    const staff$ = this.httpClient.get<Staff>(`/api/staff/${id}`);

    console.log('initializing');

    staff$.subscribe((staff) => {
      this.person = staff;
      console.log('staff ', staff);
      console.log('person ', this.person);

      const bookings$ = this.httpClient.get<Booking[]>('/api/bookings');
      const staff_booking$ = this.httpClient.get<Staff_Booking[]>('/api/booking-staff');
      const booking_room$ = this.httpClient.get<Booking_Room[]>('/api/booking-room');
      const staff$ = this.httpClient.get<Staff[]>('/api/staff');
      const tours$ = this.httpClient.get<Tour[]>('/api/tours');

      forkJoin([bookings$, staff_booking$, staff$, booking_room$, tours$]).subscribe(
        ([bookings, staff_booking, staff, booking_room, tours]: [
          Booking[],
          Staff_Booking[],
          Staff[],
          Booking_Room[],
          Tour[]
        ]) => {
          this.bookings = this.formatBookings([bookings, staff_booking, staff, booking_room]);
          console.log('bookings ', this.bookings);
          this.cRef.detectChanges();
          this.staff = staff;
          this.tours = tours;
        }
      );
    });
  }

  formatBookings([bookings, staff_booking, staff, booking_room]: [
    Booking[],
    Staff_Booking[],
    Staff[],
    Booking_Room[]
  ]) {
    console.log('this bookings', this.bookings);
    bookings.forEach((booking) => {
      const idStaff = staff_booking.find((link) => link.bookingId === booking.id)?.staffId;
      booking.inCharge = staff.find((staff) => staff.id === idStaff);
      booking.roomsNums = booking_room.filter((link) => link.bookingId == booking.id).map((br) => br.roomId);
    });
    console.log('returning', bookings);
    return bookings;
  }
}
