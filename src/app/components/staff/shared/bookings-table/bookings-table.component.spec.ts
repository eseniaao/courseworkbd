import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingsTableComponent } from './bookings-table.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BookingsTableModule } from './bookings-table.module';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Booking, Staff, Staff_Booking } from '../../../../shared/interfaces';

const mockStaff: Staff = {
  id: 1,
  firstName: 'firstName',
  lastName: 'lastName',
  rating: 1,
  salary: 1,
  positionId: 10,
};

const mockStaff2: Staff = {
  id: 2,
  firstName: 'firstName',
  lastName: 'lastName',
  rating: 1,
  salary: 1,
  position: 'Cleaning Manager',
};

const mockBooking: Booking = {
  id: 1,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
  inCharge: mockStaff,
};

const mockBooking2: Booking = {
  id: 2,
  arrivalDate: new Date(),
  departureDate: new Date(),
  bill: 1,
};

const mockBookingStaff: Staff_Booking = {
  id: 1,
  staffId: 1,
  bookingId: 1,
};

describe('BookingsTableComponent', () => {
  let component: BookingsTableComponent;
  let fixture: ComponentFixture<BookingsTableComponent>;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BookingsTableModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsTableComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('method "ngOnInit"', () => {
    it('should invoke get http request for staff and booking-staff', () => {
      // Arrange
      const httpSpy = spyOn(httpClient, 'get')
        .withArgs(`/api/staff`)
        .and.returnValue(of([mockStaff]))
        .withArgs(`/api/booking-staff`)
        .and.returnValue(of([mockBookingStaff]));

      // Act
      component.initialize();

      // Assert
      expect(httpMock.expectOne({ method: 'get', url: `/api/booking-staff` })).toBeTruthy();
      expect(httpMock.expectOne({ method: 'get', url: `/api/staff` })).toBeTruthy();

      expect(component.staff).toEqual([mockStaff]);
      expect(component.bookingStaff).toEqual([mockBookingStaff]);
    });
  });

  describe('method "filterBookings', () => {
    it('method "filterBookings" should return only bookings with staffId = personId if positionId = 10', () => {
      // Arrange
      component.bookings = [mockBooking, mockBooking2];
      component.person = mockStaff;

      // Act
      component.filterBookings();

      // Assert
      expect(component.bookings).toEqual([mockBooking]);
    });

    it('method "filterBookings" should return all bookings if person.position is Cleaning Manager', () => {
      // Arrange
      component.bookings = [mockBooking, mockBooking2];
      component.person = mockStaff2;

      // Act
      component.filterBookings();

      // Assert
      expect(component.bookings).toEqual([mockBooking, mockBooking2]);
    });
  });

  describe('method "updateCharged"', () => {
    it('should update booking-staff connection if inCharge person is changed', () => {
      // Arrange
      component.form.patchValue({ inCharge: mockStaff2 });
      component.bookingStaff = [mockBookingStaff];

      // Act
      component.updateCharged(mockBooking);

      // Assert
      const { request } = httpMock.expectOne({ method: 'patch', url: '/api/booking-staff' });
      expect(request.method).toBe('PATCH');
      expect(request.body).toEqual({ staffId: mockStaff2.id, bookingId: mockBooking.id });
    });

    it('should create booking-staff connection if inCharge person did not exist', () => {
      // Arrange
      component.form.patchValue({ inCharge: mockStaff2 });
      component.bookingStaff = [mockBookingStaff];

      // Act
      component.updateCharged(mockBooking2);

      // Assert
      const { request } = httpMock.expectOne({ method: 'post', url: '/api/booking-staff' });
      expect(request.method).toBe('POST');
      expect(request.body).toEqual({ staffId: mockStaff2.id, bookingId: mockBooking2.id });
    });
  });
});
