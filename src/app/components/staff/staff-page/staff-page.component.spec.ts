import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StaffPageComponent } from './staff-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { StaffPageModule } from './staff-page.module';
import { Router } from '@angular/router';
import { Position, Staff } from '../../../shared/interfaces';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

const mockStaff: Staff = {
  id: 0,
  firstName: 'firstName',
  lastName: 'lastName',
  rating: 1,
  salary: 1,
  positionId: 1,
};

const mockPosition: Position = {
  id: 1,
  basicSalary: 100,
  name: 'position',
};

describe('StaffPageComponent', () => {
  let component: StaffPageComponent;
  let fixture: ComponentFixture<StaffPageComponent>;
  let router: Router;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StaffPageModule, RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPageComponent);
    router = TestBed.inject(Router);
    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('invoke method "submit"', () => {
    it('should return undefined if form is empty', async () => {
      // Act
      const result = await component.submit();

      // Assert
      expect(result).toBe(undefined);
    });

    it('should invoke router navigation if person is really staff', async () => {
      // Arrange
      const routerSpy = spyOn(router, 'navigate');
      component.staff.push(mockStaff);
      component.form.setValue({ firstName: mockStaff.firstName, lastName: mockStaff.lastName });

      // Act
      await component.submit();

      // Assert
      expect(routerSpy).toHaveBeenCalledWith(['staff', 0]);
      expect(component.showStaffPage).toBe(true);
    });

    it('shouldn`t invoke router navigation if staff doesn`t exist and set invalidPassword property to true', async () => {
      // Arrange
      const routerSpy = spyOn(router, 'navigate');

      component.form.setValue({ firstName: mockStaff.firstName, lastName: mockStaff.lastName });

      // Act
      await component.submit();

      // Assert
      expect(routerSpy).not.toHaveBeenCalled();
      expect(component.invalidPassword).toBe(true);
    });
  });

  it('method "initialize" should invoke get http requests for staff and positions', () => {
    // Arrange
    spyOn(httpClient, 'get')
      .withArgs('/api/positions')
      .and.returnValue(of([mockPosition]))
      .withArgs('/api/staff')
      .and.returnValue(of([mockStaff]));

    // Act
    component.initialize();

    // Assert
    expect(httpMock.expectOne({ method: 'get', url: '/api/positions' })).toBeTruthy();
    expect(httpMock.expectOne({ method: 'get', url: '/api/staff' })).toBeTruthy();

    expect(component.positions).toEqual([mockPosition]);
    expect(component.staff).toEqual([
      {
        id: 0,
        firstName: 'firstName',
        lastName: 'lastName',
        rating: 1,
        salary: 1,
        positionId: 1,
        position: 'position',
      },
    ]);
  });
});
